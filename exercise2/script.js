const BASE_URL = 'https://swapi.co/api/people/';
let swData = [];

function getPeople() {
  doRequest(BASE_URL, /* needs callback */)
}

function loadMore() {

}

function showOnPage(response) {
  /* 1. log response in console */
  /* 2. parse to html */
  /* 3. add results to html */
}

function charactersToHTML(characters) {
  /* finalize the string */
  let htmlString = '';
  characters.forEach(character => {
    htmlString += `<div class="person">
    <h2>/* here comes the name */</h2>
    <p>Birth: /* here comes the birth date */</p>
    <p>Gender: /* here comes the gender */</p>
    </div>`
  });
  return htmlString;
}

function doRequest(url, callback) {
  let xhttp = new XMLHttpRequest();
  xhttp.open("GET", url, true);
  xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.addEventListener('load', (data) => {
    callback(JSON.parse(data.target.response));
  });
  xhttp.send();
}