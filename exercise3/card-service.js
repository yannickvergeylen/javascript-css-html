class CardService {
  constructor() {
  }

  getShuffledCards(callback){

    function doRequest(url, cb) {
      let xhttp = new XMLHttpRequest();
      xhttp.open("GET", url, true);
      xhttp.setRequestHeader("Content-type", "application/json");
      xhttp.addEventListener('load', (data) => {
        cb(JSON.parse(data.target.response));
      });
      xhttp.send();
    }

    function shuffle(cards) {
      /* fisher-yates */
      let currentIndex = cards.length, temporaryValue, randomIndex;

      while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = cards[currentIndex];
        cards[currentIndex] = cards[randomIndex];
        cards[randomIndex] = temporaryValue;
      }
      return cards;
    }

    function onReceived(cards) {
      callback(shuffle(cards.concat(cards)));
    }



    doRequest('memory-cards.json',onReceived)
  }


}